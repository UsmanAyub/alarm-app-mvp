package com.example.alarmmvp.activities.presenter;

public interface AddPresenter {

    public void alarmSuccess(String hour, String minute);

}
