package com.example.alarmmvp.activities.presenter;

public interface MainPresenter {

    public void populateList(String title, String hours, String minutes, String days);

}
