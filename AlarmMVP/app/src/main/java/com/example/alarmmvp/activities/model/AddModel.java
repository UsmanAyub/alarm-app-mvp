package com.example.alarmmvp.activities.model;

import com.example.alarmmvp.activities.presenter.AddPresenter;
import com.example.alarmmvp.activities.view.AddView;

public class AddModel implements AddPresenter {

    AddView addView;

    public AddModel(AddView aView) {
        addView = aView;
    }

    @Override
    public void alarmSuccess(String hour, String minute) {
        if (hour != null && minute != null)
        {
            addView.alarmSet();
        }

    }
}
